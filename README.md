# A\* Visualiser

A visualiser for A\* Path finding algorithm using pygame with focus on readability and clarity.

![](scr-1.png)
![](scr-2.png)

This project goes with modular approach to build the visualiser by separating the following into individual modules
- Algorithm
- Drawing facilities
- Nodes
- Colors Used

For consistency with my setup, I used [gruvbox color scheme](https://github.com/morhetz/gruvbox). Feel free to change it to suit your needs

To run the project, simply type `python3 astar.py`. Make sure you have `pygame` installed

The represent the following
| Node Type | Color |
| :---: | :---: |
| Open Node | Green |
| Closed Node | Red |
| Part of Path | Purple |
| Start Node | Orange |
| Destination Node | Aqua |
| Wall | Black |

## What is A\* algorithm?
A\* is one of the famous path finding algorithms which finds the shortest path using a heuristic. Unlike other path finding algorithms, 
the heuristic determines the priority of each node. This is calculated as

```
F(x) = G(x) + H(x)

  F(x) => Resultant Cost
  G(x) => Cost of Current Node from the start point
  H(x) => Cost to Current Node when moving towards the end point with heuristic
```

The minimum cost will be preferred and will be the most likely to be checked in the next iteration.
For further info, you can consult [this article from GeeksForGeeks](https://www.geeksforgeeks.org/a-search-algorithm/).
