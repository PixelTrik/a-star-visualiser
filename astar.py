import pygame
import astar_draw as graph
import node
import algorithm

SIZE = 700
WIN = pygame.display.set_mode((SIZE, SIZE))
pygame.display.set_caption("A* Path Finder")

# Main segment
ROWS = 50
grid = graph.genGrid(ROWS, SIZE)

start = end = None
running = True

while running:
    graph.draw(WIN, grid, ROWS, SIZE)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        mouseEvent = pygame.mouse.get_pressed()

        if mouseEvent[0]:
            pos = pygame.mouse.get_pos()
            row, col = graph.getClickedPos(pos, ROWS, SIZE)
            node = grid[row][col]

            if not start and node != end:
                start = node
                start.setStart()

            elif not end and node != start:
                end = node
                end.setEnd()

            elif node != start and node != end:
                node.setWall()
        
        if mouseEvent[2]:
            pos = pygame.mouse.get_pos()
            row, col = graph.getClickedPos(pos, ROWS, SIZE)
            node = grid[row][col]

            node.reset()
            if node == start:
                start = None
            elif node == end:
                end = None

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE and start and end:
                for row in grid:
                    for node in row:
                        node.getNeighbors(grid)

                algorithm.astar(lambda: graph.draw(WIN, grid, ROWS, SIZE), grid, start, end)

            if event.key == pygame.K_c:
                start = end = None
                grid = graph.genGrid(ROWS, SIZE)

pygame.quit()
